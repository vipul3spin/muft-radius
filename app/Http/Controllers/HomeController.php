<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use Cookie;
use App\Models\Employee;
use App\Models\Location;
use App\User;

use Entrust;


/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $roleCount = \App\Role::count();
		if($roleCount != 0) {
			if($roleCount != 0) {
				//return view('home');
                return redirect('/admin');
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}
    }

    public function chk_location() {
        
        $auth_loc = 13;
        $auth_user_id = 2;

        $emp_id = User::where('id',$auth_user_id)->value("context_id");
        $location_ids = Employee::where('id',$emp_id)->value("location_id");
        $loca = array_map('intval', json_decode($location_ids));

        $flg = false;
        if (in_array($auth_loc,$loca)) {
                $flg = true;
        } else {
                $flg = false;
        }

        return $flg;
    }

    public function loc_create(Request $request){

        Cookie::queue(Cookie::forget('loc_id'));

        $subdomain = $request->subdomain;
        $expires = 60 * 24 * 30;

        if($subdomain <> "my") {

            $location_data = Location::where( 'subdomain', $subdomain )->where( 'status', 1 )->first();

            if(! $location_data) {
                return "Location Not Found";
            } else {
                $loc_id = $location_data->id;
                Cookie::queue('loc_id', $loc_id, $expires);
            }
        } elseif($subdomain == "my") {
            $loc_id = 0;
            Cookie::queue('loc_id', $loc_id, $expires);
        } else {
            return "Location Not Found";
        }

        $flg = false;

        if(Auth::check()) {

            if(Entrust::hasRole('SUPER_ADMIN')) {
                return redirect('/admin/dashboard')->with('success','Welcome Admin');
            } else {    
                $auth_user_id = Auth::user()->id;
                $emp_id = User::where('id',$auth_user_id)->value("context_id");
                $location_ids = Employee::where('id',$emp_id)->value("location_id");
                $loca = array_map('intval', json_decode($location_ids));

                if($loc_id == 0){
                    if(count($loca) == 1){
                        Cookie::queue(Cookie::forget('loc_id'));
                        $loc_id = $loca[0];
                        Cookie::queue('loc_id', $loc_id, $expires);                        
                    } else {
                        $sel_loc = Location::whereIn('id',$loca)->get();
                        return view('auth.goto_loc',compact('sel_loc'));
                    }
                }

                if (in_array($loc_id,$loca)) {
                    $flg = true;
                } else {
                    $flg = false;
                }
            }
        } else {
            return redirect('/login')->with('error','Permission denied');
        }

        if($flg){
            return redirect('/admin/dashboard')->with('success','Welcome '.Auth::user()->name);
        } else {
            //return "Location Not Accessible <a href='".url('/logout')."'>logout</a>";
            Auth::logout();
            return redirect('/login')->with('error','Permission denied');
        }
 
    }

    public function goto_location(Request $request)
    {
        if(Auth::check()){
            if($request->sel_loc <> 0 && $request->sel_loc <> "" && $request->sel_loc <> NULL){
                Cookie::queue(Cookie::forget('loc_id'));
                $loc_id = $request->sel_loc;
                $expires = 60 * 24 * 30;
                Cookie::queue('loc_id', $loc_id, $expires);
                return redirect('/admin/dashboard')->with('success','Welcome '.Auth::user()->name);
            } else {
                return redirect()->back()->with('error','Permission denied');
            }

        } else {
            Auth::logout();
            return redirect('/login')->with('error','Permission denied');            
        }
    }

}
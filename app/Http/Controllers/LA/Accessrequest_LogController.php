<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Carbon\Carbon;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

class Accessrequest_LogController extends Controller
{
	public $show_action = false;
	public $view_col = 'Username';
	public $listing_cols = ['Id', 'DateTime', 'NAS IP', 'Client MAC', 'Username', 'Reply', 'Massage'];
	
	public function __construct() {
		parent::__construct();
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				//$this->listing_cols = ModuleFields::listingColumnAccessScan('Email_Stats', $this->listing_cols);
				return $next($request);
			});
		} else {
			//$this->listing_cols = ModuleFields::listingColumnAccessScan('Email_Stats', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Email_Stats.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//$module = Module::get('Email_Stats');
		
		///if(Module::hasAccess($module->id)) {
			return View('la.accessrequest_log.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols
				//'module' => $module
			]);
		//} else {
       //     return redirect(config('laraadmin.adminRoute')."/");
       // }
	}

	/**
	 * Show the form for creating a new email_category.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		if($this->uloc_id == 0) {
			$values = DB::table('radpostauth')->select('id','created_at','nas_ip','mac','username','reply','reply_msg')->orderBy('created_at', 'DESC');
		} else {
			$values_sub = DB::table('subscribers')->select('username')->where('loc_id',$this->uloc_id)->whereNull('deleted_at');

			$values = DB::table('radpostauth')->select('id','created_at','nas_ip','mac','username','reply','reply_msg')->whereIn('username',$values_sub,'and')->orderBy('created_at', 'DESC');
		}
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		//$fields_popup = ModuleFields::getModuleFields('Email_Stats');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($col == $this->view_col) {
					//$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/email_stats/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
				if($col == 'DateTime') {
					$data->data[$i][$j] = Carbon::createFromFormat('Y-m-d H:i:s', $data->data[$i][$j])->format('d-m-Y h:i:s A');
				}	

			}
		}
		$out->setData($data);
		return $out;
	}

}

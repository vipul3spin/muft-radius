<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Package;

use App\Models\Radius\Radgroupcheck;
use App\Models\Radius\Radgroupreply;

class PackagesController extends Controller
{
	public $show_action = true;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name', 'valid_for', 'bandwidth_up', 'bandwidth_down'];
	
	public function __construct() {
		parent::__construct();
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Packages', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Packages', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Packages.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Packages');
		
		if(Module::hasAccess($module->id)) {
			return View('la.packages.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Access denied');
        }
	}

	/**
	 * Show the form for creating a new package.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created package in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Packages", "create")) {
		
			$rules = Module::validateRules("Packages", $request);

			if(is_numeric($request->valid_for) && is_numeric($request->bandwidth_down) && is_numeric($request->bandwidth_up)){

				$validator = Validator::make($request->all(), $rules);
				
				if ($validator->fails()) {
					return redirect()->back()->withErrors($validator)->withInput();
				}
				
				$insert_id = Module::insert("Packages", $request);

				$radgroupname = $request->name;

				$radgroupcheck = DB::table('radgroupcheck')->insertGetId([
					'groupname' => $radgroupname,
					'attribute' => 'Auth-Type',
					'op' => ':=',
					'value' => 'Accept'
				]);

				$download = $request->bandwidth_down;
				$upload = $request->bandwidth_up;
				$mk_rate = $download."M/".$upload."M";

				$radgroupreply1 = DB::table('radgroupreply')->insertGetId([
					'groupname' => $radgroupname,
					'attribute' => 'Mikrotik-Rate-Limit',
					'op' => ':=',
					'value' => $mk_rate
				]);

				$radgroupreply2 = DB::table('radgroupreply')->insertGetId([
					'groupname' => $radgroupname,
					'attribute' => 'Fall-Through',
					'op' => '=',
					'value' => 'Yes'
				]);

				return redirect()->route(config('laraadmin.adminRoute') . '.packages.index')->with('success','Package created');

			} else {
				return redirect()->back()->with('error',"Please enter Validity & Bandwidth in figurs");
			}
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Display the specified package.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Packages", "view")) {
			
			$package = Package::find($id);
			if(isset($package->id)) {
				$module = Module::get('Packages');
				$module->row = $package;
				
				return view('la.packages.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('package', $package);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("package"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Show the form for editing the specified package.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Packages", "edit")) {			
			$package = Package::find($id);
			if(isset($package->id)) {	
				$module = Module::get('Packages');
				
				$module->row = $package;
				
				return view('la.packages.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('package', $package);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("package"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Update the specified package in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Packages", "edit")) {
			
			$rules = Module::validateRules("Packages", $request, true);

			if(is_numeric($request->valid_for) && is_numeric($request->bandwidth_down) && is_numeric($request->bandwidth_up)){

				$validator = Validator::make($request->all(), $rules);
				
				if ($validator->fails()) {
					return redirect()->back()->withErrors($validator)->withInput();;
				}
				
				$insert_id = Module::updateRow("Packages", $request, $id);

				$radgroupname = $request->name;				

				if($request->old_name <> $request->name){

				$radgroupcheck = DB::table('radgroupcheck')->where(['groupname' => $request->old_name])->update(['groupname' => $radgroupname]);

				$radgroupreply = DB::table('radgroupreply')->where(['groupname' => $request->old_name])->update(['groupname' => $radgroupname]);
				}

				$download = $request->bandwidth_down;
				$upload = $request->bandwidth_up;
				$mk_rate = $download."M/".$upload."M";

				$radgroupreply = DB::table('radgroupreply')->where(['groupname'=>$radgroupname,'attribute'=>'Mikrotik-Rate-Limit'])->update(['value' => $mk_rate]);	
	
				return redirect()->route(config('laraadmin.adminRoute') . '.packages.index')->with('success','Package updated');

			} else {
				return redirect()->back()->with('error',"Please enter Validity & Bandwidth in figurs");
			}			
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Remove the specified package from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Packages", "delete")) {
			Package::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.packages.index')->with('success','Package deleted');
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		if($this->uloc_id == 0) {
			$values = DB::table('packages')->select($this->listing_cols)->whereNull('deleted_at');
		} else {
			$values = DB::table('packages')->select($this->listing_cols)->where('loc_id',$this->uloc_id)->whereNull('deleted_at');
		}

		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Packages');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/packages/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Packages", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/packages/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Packages", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.packages.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}

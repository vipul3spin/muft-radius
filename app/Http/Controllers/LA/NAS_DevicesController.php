<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\NAS_Device;
use Entrust;

class NAS_DevicesController extends Controller
{
	public $show_action = true;
	public $view_col = 'nas_ip';
	public $listing_cols = ['id', 'nas_ip', 'short_name', 'nas_type'];
	
	public function __construct() {
		parent::__construct();
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('NAS_Devices', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('NAS_Devices', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the NAS_Devices.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('NAS_Devices');
		
		if(Module::hasAccess($module->id)) {
			return View('la.nas_devices.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Access denied');
        }
	}

	/**
	 * Show the form for creating a new nas_device.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created nas_device in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("NAS_Devices", "create")) {
		
			$rules = Module::validateRules("NAS_Devices", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}

			if($request->nas_ip <> "0.0.0.0/0" && $request->nas_ip <> "0.0.0.0"){
				$nas_device_details = DB::table('nas_devices')->where('nas_ip',$request->nas_ip)->whereNull('deleted_at')->get();
				$nas_device = DB::table('nas')->where('nasname',$request->nas_ip)->get();

				if(empty($nas_device_details) && empty($nas_device)){
					$insert_id = Module::insert("NAS_Devices", $request);

					$nas_ins = DB::table('nas')->insertGetId([
						'nasname' => $request->nas_ip,
						'shortname' => $request->short_name,
						'type' => $request->nas_type,
						'ports' => 0,
						'secret' => $request->secret,
						'description' => $request->description
					]);

					//exec("sudo /etc/init.d/freeradius restart", $output);

				} else{
					return redirect()->back()->with('info',"NAS device already registered");
				}
			} else {
				return redirect()->route(config('laraadmin.adminRoute') . '.nas_devices.index')->with('error','NAS IP Not allowed');
			}

			return redirect()->route(config('laraadmin.adminRoute') . '.nas_devices.index')->with('success','Nas device created');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Display the specified nas_device.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("NAS_Devices", "view")) {
			
			$nas_device = NAS_Device::find($id);
			if(isset($nas_device->id)) {
				$module = Module::get('NAS_Devices');
				$module->row = $nas_device;
				
				return view('la.nas_devices.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('nas_device', $nas_device);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("nas_device"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Show the form for editing the specified nas_device.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("NAS_Devices", "edit")) {			
			$nas_device = NAS_Device::find($id);
			if(isset($nas_device->id)) {	
				$module = Module::get('NAS_Devices');
				
				$module->row = $nas_device;
				
				return view('la.nas_devices.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('nas_device', $nas_device);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("nas_device"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Update the specified nas_device in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("NAS_Devices", "edit")) {
			
			$rules = Module::validateRules("NAS_Devices", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
		
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			if($request->nas_ip <> "0.0.0.0/0" && $request->nas_ip <> "0.0.0.0"){
			
				$nas_device_details = DB::table('nas_devices')->where('id',$id)->whereNull('deleted_at')->get();
				$nas_device = DB::table('nas')->where('nasname',$nas_device_details[0]->nas_ip)->get();

				if(!empty($nas_device_details) && !empty($nas_device)){
					$insert_id = Module::updateRow("NAS_Devices", $request, $id);

					$nas_ins = DB::table('nas')->where('nasname',$nas_device_details[0]->nas_ip)->update([
						'nasname' => $request->nas_ip,
						'shortname' => $request->short_name,
						'type' => $request->nas_type,
						'ports' => 0,
						'secret' => $request->secret,
						'description' => $request->description
					]);

					//exec("sudo /etc/init.d/freeradius restart", $output);
				} else{
					return redirect()->back()->with('info',"NAS device not registered");
				}
			} else {
				return redirect()->route(config('laraadmin.adminRoute') . '.nas_devices.index')->with('error','NAS IP Not allowed');
			}

			return redirect()->route(config('laraadmin.adminRoute') . '.nas_devices.index')->with('success','NAS device updated');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Remove the specified nas_device from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("NAS_Devices", "delete")) {
			NAS_Device::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.nas_devices.index')->with('success','NAS device deleted');
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		if($this->uloc_id == 0) {
			$values = DB::table('nas_devices')->select($this->listing_cols)->whereNull('deleted_at');
		} else {
			$values = DB::table('nas_devices')->select($this->listing_cols)->where('loc_id',$this->uloc_id)->whereNull('deleted_at');
		}

		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('NAS_Devices');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/nas_devices/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("NAS_Devices", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/nas_devices/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("NAS_Devices", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.nas_devices.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}

<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Carbon\Carbon;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Package_Sale;
use App\Models\Subscriber;
use App\Models\Package;
use App\Models\Radius\Radcheck;
use App\Models\Radius\Radusergroup;

class Package_SalesController extends Controller
{
	public $show_action = true;
	public $view_col = 'subs_id';
	public $listing_cols = ['id', 'subs_id', 'package_id', 'sale_date'];
	
	public function __construct() {
		parent::__construct();
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Package_Sales', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Package_Sales', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Package_Sales.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Package_Sales');
		
		if(Module::hasAccess($module->id)) {
			return View('la.package_sales.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Access denied');
        }
	}

	/**
	 * Show the form for creating a new package_sale.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created package_sale in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		if(Module::hasAccess("Package_Sales", "create")) {
		
			$rules = Module::validateRules("Package_Sales", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Package_Sales", $request);

			$subscriber_details = Subscriber::find($request->subs_id);
			$package_details = Package::find($request->package_id);

			$sub_username = $subscriber_details->username;
			$package_name = $package_details->name;
			$plan_expiry = Carbon::now()->addDays($package_details->valid_for)->format('d M Y H:i');

			$radchk = DB::table('radcheck')->where(['username'=>$sub_username,'attribute'=>'Expiration'])->get();

			if(!empty($radchk)){
				$radcheck_Update = DB::table('radcheck')->where(['username' => $sub_username,'attribute' => 'Expiration'])->update(['value' => $plan_expiry]);
			} else {
				$radcheck_ins = DB::table('radcheck')->insertGetId([
					'username' => $sub_username,
					'attribute' => 'Expiration',
					'op' => ':=',
					'value' => $plan_expiry
				]);	
			}

			$radusrgroup = DB::table('radusergroup')->where('username',$sub_username)->get();

			if(!empty($radusrgroup)){
				$radusrgroup_Update = DB::table('radusergroup')->where(['username' => $sub_username])->update(['groupname' => $package_name]);
			} else {
				$radusrgroup_ins = DB::table('radusergroup')->insertGetId([
					'username' => $sub_username,
					'groupname' => $package_name,
					'priority' => '0'
				]);	
			}

			return redirect()->route(config('laraadmin.adminRoute') . '.package_sales.index')->with('success','Package added to subscriber');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Display the specified package_sale.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Package_Sales", "view")) {
			
			$package_sale = Package_Sale::find($id);
			if(isset($package_sale->id)) {
				$module = Module::get('Package_Sales');
				$module->row = $package_sale;
				
				return view('la.package_sales.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('package_sale', $package_sale);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("package_sale"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Show the form for editing the specified package_sale.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Package_Sales", "edit")) {			
			$package_sale = Package_Sale::find($id);
			if(isset($package_sale->id)) {	
				$module = Module::get('Package_Sales');
				
				$module->row = $package_sale;
				
				return view('la.package_sales.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('package_sale', $package_sale);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("package_sale"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Update the specified package_sale in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Package_Sales", "edit")) {
			
			$rules = Module::validateRules("Package_Sales", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Package_Sales", $request, $id);

			$subscriber_details = Subscriber::find($request->subs_id);
			$package_details = Package::find($request->package_id);

			$sub_username = $subscriber_details->username;
			$package_name = $package_details->name;
			$plan_expiry = Carbon::now()->addDays($package_details->valid_for)->format('d M Y H:i');

			$radchk = DB::table('radcheck')->where(['username'=>$sub_username,'attribute'=>'Expiration'])->get();

			if(!empty($radchk)){
				$radcheck_Update = DB::table('radcheck')->where(['username' => $sub_username,'attribute' => 'Expiration'])->update(['value' => $plan_expiry]);
			} else {
				$radcheck_ins = DB::table('radcheck')->insertGetId([
					'username' => $sub_username,
					'attribute' => 'Expiration',
					'op' => ':=',
					'value' => $plan_expiry
				]);	
			}

			$radusrgroup = DB::table('radusergroup')->where('username',$sub_username)->get();

			if(!empty($radusrgroup)){
				$radusrgroup_Update = DB::table('radusergroup')->where(['username' => $sub_username])->update(['groupname' => $package_name]);
			} else {
				$radusrgroup_ins = DB::table('radusergroup')->insertGetId([
					'username' => $sub_username,
					'groupname' => $package_name,
					'priority' => '0'
				]);	
			}			
			
			return redirect()->route(config('laraadmin.adminRoute') . '.package_sales.index')->with('success','Subscriber package updated');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Remove the specified package_sale from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Package_Sales", "delete")) {
			//Package_Sale::find($id)->delete();
			
			// Redirecting to index() method
			//return redirect()->route(config('laraadmin.adminRoute') . '.package_sales.index')->with('success','Package removed from subscriber account');

			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		if($this->uloc_id == 0) {
			$values = DB::table('package_sales')->select($this->listing_cols)->whereNull('deleted_at');
		} else {
			$values = DB::table('package_sales')->select($this->listing_cols)->where('loc_id',$this->uloc_id)->whereNull('deleted_at');
		}
		//$values = DB::table('package_sales')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Package_Sales');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/package_sales/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Package_Sales", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/package_sales/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Package_Sales", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.package_sales.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}

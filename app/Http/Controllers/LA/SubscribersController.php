<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Cookie;
use Validator;
use Datatables;
use Carbon\Carbon;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Subscriber;
use Entrust;

class SubscribersController extends Controller
{
	public $show_action = true;
	public $view_col = 'username';
	public $listing_cols = ['id', 'name', 'username','city', 'mobile', 'email', 'status'];
	//public $listing_cols = ['id', 'name', 'username', 'pass', 'confirm_pass', 'addr', 'city', 'state', 'country', 'zip', 'mobile', 'email', 'status', 'fixed_ip', 'bind_mac', 'mac_addr', 'loc_id'];
    public $listing_history = ['Date Time', 'NAS IP', 'Device Mac', 'Client IP', 'Session Time', 'Download', 'Upload'];	
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Subscribers', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Subscribers', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Subscribers.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Subscribers');
		
		if(Module::hasAccess($module->id)) {
			return View('la.subscribers.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Access denied');
        }
	}

	/**
	 * Show the form for creating a new subscriber.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created subscriber in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Subscribers", "create")) {
		
			$rules = Module::validateRules("Subscribers", $request);

			if($request->username <> "" && $request->username <> NULL) {

				$username = $request->username;

				$redchk_valid = DB::table('radcheck')->where('username',$username)->get();

				if(empty($redchk_valid)){

					if($request->pass == $request->confirm_pass) {
				
						$validator = Validator::make($request->all(), $rules);
						
						if ($validator->fails()) {
							return redirect()->back()->withErrors($validator)->withInput();
						}
						
						$insert_id = Module::insert("Subscribers", $request);

						if($request->hasFile('kyc_doc')){
							$kyc_name = "kyc_".$insert_id;
							$kyc_file = $request->file('kyc_doc');
							$filename = $kyc_file->getClientOriginalName();
							//$extnpan = $kyc_file->getMimeType();//pathinfo($request->panp , PATHINFO_EXTENSION);
		
							$extnpan = substr(strrchr($filename, '.'), 1);
		
							if($request->hasFile('kyc_doc') && ($extnpan == "jpg" || $extnpan == "JPG" || $extnpan == "png" || $extnpan == "png" || $extnpan == "jpeg" || $extnpan == "JPEG" || $extnpan == "pdf" || $extnpan == "PDF")){
								$panpname = $kyc_name.'.'.$extnpan;
								$panName = storage_path('kyc').'/'.$panpname;
								move_uploaded_file($request->kyc_doc, $panName);
								$request['kyc_doc'] = $panpname;
							}
						}						

						$radcheck1 = DB::table('radcheck')->insertGetId([
							'username' => $username,
							'attribute' => 'Cleartext-Password',
							'op' => ':=',
							'value' => $request->confirm_pass
						]);

						$radcheck2 = DB::table('radcheck')->insertGetId([
							'username' => $username,
							'attribute' => 'Expiration',
							'op' => ':=',
							'value' => Carbon::now()->subDays(1)->format('d M Y H:i')
						]);

						if($request->fixed_ip <> "" && $request->fixed_ip <> null){
							$fix_ip = $request->fixed_ip;
							$radreply = DB::table('radreply')->insertGetId([
								'username' => $username,
								'attribute' => 'Framed-IP-Address',
								'op' => ':=',
								'value' => $fix_ip
							]);							
						}							
						
						return redirect()->route(config('laraadmin.adminRoute') . '.subscribers.index')->with('success','Subscriber account created');

					} else {
						return redirect()->back()->with('error',"Please enter valid password");
					}
				} else {
					return redirect()->back()->with('error',"Username already exists");
				}
			} else{
				return redirect()->back()->with('error',"Please enter valid username");
			}			
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Display the specified subscriber.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Subscribers", "view")) {
			$subscriber = Subscriber::find($id);
			if(isset($subscriber->id)) {
				if($subscriber->loc_id == $this->uloc_id || $this->uloc_id == 0){
					$module = Module::get('Subscribers');
					$module->row = $subscriber;

					return view('la.subscribers.show', [
						'module' => $module,
						'view_col' => $this->view_col,
						'listing_cols' => $this->listing_history,
						'no_header' => true,
						'no_padding' => "no-padding"
					])->with('subscriber', $subscriber);
				} else {
					return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
				}
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("subscriber"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Show the form for editing the specified subscriber.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Subscribers", "edit")) {			
			$subscriber = Subscriber::find($id);
			if(isset($subscriber->id)) {	
				$module = Module::get('Subscribers');
				
				$module->row = $subscriber;
				
				return view('la.subscribers.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('subscriber', $subscriber);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("subscriber"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified subscriber in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Subscribers", "edit")) {
			
			$rules = Module::validateRules("Subscribers", $request, true);

			if($request->pass == $request->confirm_pass){
			
				$validator = Validator::make($request->all(), $rules);
				
				if ($validator->fails()) {
					return redirect()->back()->withErrors($validator)->withInput();
				}

				if($request->hasFile('kyc_doc')){
					$kyc_name = "kyc_".$id;
					$kyc_file = $request->file('kyc_doc');
					$filename = $kyc_file->getClientOriginalName();
					//$extnpan = $kyc_file->getMimeType();//pathinfo($request->panp , PATHINFO_EXTENSION);

					$extnpan = substr(strrchr($filename, '.'), 1);

					if($request->hasFile('kyc_doc') && ($extnpan == "jpg" || $extnpan == "JPG" || $extnpan == "png" || $extnpan == "png" || $extnpan == "jpeg" || $extnpan == "JPEG" || $extnpan == "pdf" || $extnpan == "PDF")){
						$panpname = $kyc_name.'.'.$extnpan;
						$panName = storage_path('kyc').'/'.$panpname;
						move_uploaded_file($request->kyc_doc, $panName);
						$request->kyc_doc = $panpname;
					}
				}				

				$insert_id = Module::updateRow("Subscribers", $request, $id);

				if($request->old_pass <> $request->confirm_pass){
					$radcheck1 = DB::table('radcheck')->where(['username' => $request->username, 'attribute' => 'Cleartext-Password'])->update(['value' => $request->confirm_pass]);
				}

				$radreply = DB::table('radreply')->where(['username' => $request->username, 'attribute' => 'Framed-IP-Address'])->delete();

				if($request->fixed_ip <> "" && $request->fixed_ip <> null){
					$fix_ip = $request->fixed_ip;
					if(filter_var($fix_ip, FILTER_VALIDATE_IP)){
						$radreply = DB::table('radreply')->insertGetId([
							'username' => $request->username,
							'attribute' => 'Framed-IP-Address',
							'op' => ':=',
							'value' => $fix_ip
						]);
					} else {
						return redirect()->back()->with('error','Enter valid fixed ip address');
					}
				} 
				
				return redirect()->route(config('laraadmin.adminRoute') . '.subscribers.index')->with('success','Subscriber account updated');

			} else {
				return redirect()->back()->with('error',"Please enter valid password");
			}			
		} else {
			return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
		}
	}

	/**
	 * Remove the specified subscriber from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Subscribers", "delete")) {
			Subscriber::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.subscribers.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		if($this->uloc_id == 0) {
			$values = DB::table('subscribers')->select($this->listing_cols)->whereNull('deleted_at');
		} else {
			$values = DB::table('subscribers')->select($this->listing_cols)->where('loc_id',$this->uloc_id)->whereNull('deleted_at');
		}
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Subscribers');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/subscribers/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Subscribers", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/subscribers/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Subscribers", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.subscribers.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}

	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function hisajax($id)
	{
		$subscriber = Subscriber::find($id);

        if($this->uloc_id == 0 || $this->uloc_id == $subscriber->loc_id) {

            
            $username = $subscriber->username;
           
                 $values = DB::table('radacct')
                            ->select(\DB::raw('acctstarttime, nasipaddress as router, callingstationid as device, framedipaddress as ip, acctsessiontime as session, acctinputoctets as download, acctoutputoctets as upload'))
                            ->where('username','=',$username)
                            ->orderBy('acctstarttime','desc')
                            ->get();
                 
                 $data = array();
                 foreach($values as $val){
					$val->acctstarttime = Carbon::createFromFormat('Y-m-d H:i:s', $val->acctstarttime)->format('d-m-Y h:i:s A');
					$val->session = gmdate("H:i:s", $val->session);
                    $val->download = $this->convertToReadableSize($val->download);
                    $val->upload = $this->convertToReadableSize($val->upload);  

                    $data[] = (array)$val;  
                 }                

                $valuesa = collect($data);
                $out = Datatables::of($valuesa)->make();
		$data = $out->getData();
		$out->setData($data);
		return $out;   
		} else {
			return "";
		}              
	}

	public static function convertToReadableSize($size) {
		if($size > 0) {
		    $base = log($size) / log(1024);
		    $suffix = array("Bytes", "KB", "MB", "GB", "TB");
		    $f_base = floor($base);
			$dataas = round(pow(1024, $base - floor($base)), 1) ." ". $suffix[$f_base];
		} else {
			$dataas = "0";
		}
	    return $dataas;
	}	
}

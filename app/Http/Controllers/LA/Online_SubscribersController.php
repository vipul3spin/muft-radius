<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use DateTime;
use Excel;
use Validator;
use Datatables;
use Carbon\Carbon;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use Dwij\Laraadmin\Models\LAConfigs;
use Illuminate\Support\Facades\Input;

use Entrust;
use View;

class Online_SubscribersController extends Controller
{
	public $show_action = false;
	public $view_col = '';//'end_users_id';
	public $listing_cols = ['Subscriber ID', 'Router Name', 'Device MAC', 'IP', 'Login Time', 'Session Time','Download','Upload'];
        public $listing_colsa = ['ID', 'Subscriber', 'Package', 'NAS IP', 'Client IP', 'Start Time', 'Report Time','Session','Download','Upload'];
	
	public function __construct() {
		parent::__construct();
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				//$this->listing_cols = ModuleFields::listingColumnAccessScan('Subscribers', $this->listing_cols);
				return $next($request);
			});
		} else {
			//$this->listing_cols = ModuleFields::listingColumnAccessScan('Subscribers', $this->listing_cols);
		}
                $owners = DB::table('employees')->select('id','name')->whereNull('deleted_at')->get();
                View::share('owners', $owners);                
	}
	
	/**
	 * Display a listing of the Sessions.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Subscribers');
		if(Module::hasAccess($module->id)) {                     
			return View('la.subscribers.online', [
				'show_actions' => false,
				'listing_cols' => $this->listing_colsa,
				'module' => $module
			]);
		} else {
                    return redirect(config('laraadmin.adminRoute')."/dashboard")->with('error','Permission denied');
                }
	}        
        
	public function dtajax()
	{

		if($this->uloc_id == 0) {
                $values = DB::table('radacct as rc')
                    ->select(\DB::raw('rc.radacctid, rc.username, rug.groupname, rc.nasipaddress,  rc.framedipaddress, rc.acctstarttime, rc.acctupdatetime, rc.acctsessiontime, rc.acctinputoctets as download, rc.acctoutputoctets as upload'))
                    ->leftJoin('radusergroup as rug', 'rc.username', '=', 'rug.username')
                    //->where('rc.acctstarttime','>=',Carbon::today())
                    ->whereIn('rc.acctterminatecause',['0',''],'and')
                    ->orderBy('rc.acctstarttime', 'DESC')
                    ->get();
		} else {
			$values_sub = DB::table('subscribers')->select('username')->where('loc_id',$this->uloc_id)->whereNull('deleted_at');

            $values = DB::table('radacct as rc')
                ->select(\DB::raw('rc.radacctid, rc.username, rug.groupname, rc.nasipaddress,  rc.framedipaddress, rc.acctstarttime, rc.acctupdatetime, rc.acctsessiontime, rc.acctinputoctets as download, rc.acctoutputoctets as upload'))
                ->leftJoin('radusergroup as rug', 'rc.username', '=', 'rug.username')
                //->where('rc.acctstarttime','>=',Carbon::today())
                ->whereIn('rc.acctterminatecause',['0',''],'and')
                ->whereIn('rc.username',$values_sub,'and')
                ->orderBy('rc.acctstarttime', 'DESC')
                ->get();			
		}		

                 foreach($values as $val){
                     $user = $val->username;
                     	$val->pkg = $this->getuserpkg($user);
                         $val->acctstarttime = Carbon::createFromFormat('Y-m-d H:i:s', $val->acctstarttime)->format('d-m-Y h:i:s A');
                         $val->acctupdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $val->acctupdatetime)->format('d-m-Y h:i:s A');                     
                         $val->download = $this->convertToReadableSize($val->download);
                         $val->upload = $this->convertToReadableSize($val->upload);
                         $val->acctsessiontime = gmdate("H:i:s", $val->acctsessiontime);

                 }                    

                $valuesa = collect($values);
                $out = Datatables::of($valuesa)->make();
		$data = $out->getData();
		$out->setData($data);
		return $out;
	}  

	public static function convertToReadableSize($size) {
		if($size > 0) {
		    $base = log($size) / log(1024);
		    $suffix = array("Bytes", "KB", "MB", "GB", "TB");
		    $f_base = floor($base);
			$dataas = round(pow(1024, $base - floor($base)), 1) ." ". $suffix[$f_base];
		} else {
			$dataas = "0";
		}
	    return $dataas;
	}

	public static function getuserpkg($username) {
		$pkg = DB::table('radusergroup')->where(['username' => $username, 'priority' => 0])->value('groupname');

		return $pkg;
	}
        
        public function logout($id) {
            if(isset($id) && is_numeric($id)){
                $result = DB::table('radacct')->where('radacctid','=',$id)->update(['acctterminatecause'=> 'Admin-Reset', 'acctstoptime'=> Carbon::now()]);
                return redirect(config('laraadmin.adminRoute')."/active-sessions")->with('success','User logout successfully');
            } else {
                return redirect(config('laraadmin.adminRoute')."/active-sessions")->with('error','Permission denied');
            }
        }
}

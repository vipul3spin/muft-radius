<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use Auth;
use DB;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models;
use Cookie;
use DateTime;
use Entrust;
use File;

use App\User;
use App\Models\Subscriber;
use App\Models\Package;
use App\Models\Package_Sale;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $loca_id = $this->uloc_id;
        if($loca_id == 0) {
                $data['totalsub'] = $this->gettotalsubs();
                $data['activesub'] =$this->getactivesubs();
                $data['onlinesub'] = $this->getonlinesubs(); 
                $data['totalpkgs'] = $this->gettotalpkgs();
                $data['totalpkg_sale'] = $this->gettotalpkgsale();
                $data['todaypkg_sale'] = $this->gettodaypkgsale();
        } else {
                $data['totalsub'] = $this->gettotalsubs($loca_id);
                $data['activesub'] =$this->getactivesubs($loca_id);
                $data['onlinesub'] = $this->getonlinesubs($loca_id); 
                $data['totalpkgs'] = $this->gettotalpkgs($loca_id);
                $data['totalpkg_sale'] = $this->gettotalpkgsale($loca_id);
                $data['todaypkg_sale'] = $this->gettodaypkgsale($loca_id);                
        }

        return view('la.dashboard',compact(array('data')));

        //return view('la.dashboard',compact('loca_id'));
    }

    public function gettotalsubs($id = NULL)
    {
        if($id == 0){
            $values = Subscriber::count('username');
        } else {
            $values = Subscriber::where('loc_id', '=', $id)->count('username'); 
        }
        return $values;
    }   

    public function getactivesubs($id = NULL)
    {
        if($id == 0){
            $values = Subscriber::where(['status' => 'Enable'])->count('username');
        } else {
            $values = Subscriber::where(['loc_id' => $id, 'status' => 'Enable'])->count('username'); 
        }
        return $values;
    } 

    public function getonlinesubs($id = NULL)
    {
        if($this->uloc_id == 0) {
            $values = DB::table('radacct as rc')
                ->select(\DB::raw('rc.username, rug.groupname'))
                ->leftJoin('radusergroup as rug', 'rc.username', '=', 'rug.username')
                ->where('rc.acctstarttime','>=',Carbon::today())
                ->whereIn('rc.acctterminatecause',['0',''],'and')
                ->count('rc.username');
        } else {
            $values_sub = DB::table('subscribers')->select('username')->where('loc_id',$id)->whereNull('deleted_at');

            $values = DB::table('radacct as rc')
                ->select(\DB::raw('rc.username, rug.groupname'))
                ->leftJoin('radusergroup as rug', 'rc.username', '=', 'rug.username')
                ->where('rc.acctstarttime','>=',Carbon::today())
                ->whereIn('rc.acctterminatecause',['0',''],'and')
                ->whereIn('rc.username',$values_sub,'and')
                ->count('rc.username');            
        }

        return $values;
    } 

    public function gettotalpkgs($id = NULL)
    {
        if($id == 0){
            $values = Package::count('id');
        } else {
            $values = Package::where('loc_id', '=', $id)->count('id'); 
        }
        return $values;
    }  

    public function gettotalpkgsale($id = NULL)
    {
        if($id == 0){
            $values = Package_Sale::count('id');
        } else {
            $values = Package_Sale::where('loc_id', '=', $id)->count('id'); 
        }
        return $values;
    }   

    public function gettodaypkgsale($id = NULL)
    {
        if($id == 0){
            $values = Package_Sale::where(['created_at' => Carbon::today()])->count('id');
        } else {
            $values = Package_Sale::where(['created_at' => Carbon::today(), 'loc_id' => $id])->count('id'); 
        }
        return $values;
    }         
}
<?php

Route::group(['domain' => '{subdomain}.wisp.muft'], function() {
    Route::get('/admin','HomeController@loc_create');
    Route::post('/sel_location','HomeController@goto_location');
});

/* ================== Homepage ================== */
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::auth();

/* ================== Access Uploaded Files ================== */
Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

Route::get('kyc/{filename}', function ($filename)
{
    $path = storage_path('kyc/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
	$as = config('laraadmin.adminRoute').'.';
	
	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');
}       

Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {
	
	/* ================== Dashboard ================== */
	
	Route::get(config('laraadmin.adminRoute'), 'LA\DashboardController@index');
	Route::get(config('laraadmin.adminRoute'). '/dashboard', 'LA\DashboardController@index');
	
	/* ================== Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');
	
	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');
	
	/* ================== Roles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
	
	/* ================== Permissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	
	/* ================== Departments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');
	
	/* ================== Employees ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/change_password/{id}', 'LA\EmployeesController@change_password');
	
	/* ================== Locations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/locations', 'LA\LocationsController');
	Route::get(config('laraadmin.adminRoute') . '/location_dt_ajax', 'LA\LocationsController@dtajax');

	/* ================== Backups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');

	/* ================== NAS_Devices ================== */
	Route::resource(config('laraadmin.adminRoute') . '/nas_devices', 'LA\NAS_DevicesController');
	Route::get(config('laraadmin.adminRoute') . '/nas_device_dt_ajax', 'LA\NAS_DevicesController@dtajax');

	/* ================== Subscribers ================== */
	Route::resource(config('laraadmin.adminRoute') . '/subscribers', 'LA\SubscribersController');
	Route::get(config('laraadmin.adminRoute') . '/subscriber_dt_ajax', 'LA\SubscribersController@dtajax');
	Route::get(config('laraadmin.adminRoute') . '/subscriber_ajax/{id}', 'LA\SubscribersController@hisajax');
	Route::resource(config('laraadmin.adminRoute') . '/online_subscribers', 'LA\Online_SubscribersController');
	Route::get(config('laraadmin.adminRoute') . '/online_subscriber_dt_ajax', 'LA\Online_SubscribersController@dtajax');	

	/* ================== Packages ================== */
	Route::resource(config('laraadmin.adminRoute') . '/packages', 'LA\PackagesController');
	Route::get(config('laraadmin.adminRoute') . '/package_dt_ajax', 'LA\PackagesController@dtajax');

	/* ================== Package_Sales ================== */
	Route::resource(config('laraadmin.adminRoute') . '/package_sales', 'LA\Package_SalesController');
	Route::get(config('laraadmin.adminRoute') . '/package_sale_dt_ajax', 'LA\Package_SalesController@dtajax');

	/* ================== Access_Request_Logs ================== */
	Route::get(config('laraadmin.adminRoute') . '/access_req_logs', 'LA\Accessrequest_LogController@index');
	Route::get(config('laraadmin.adminRoute') . '/access_req_logs_dt_ajax', 'LA\Accessrequest_LogController@dtajax');

});
<?php

namespace App;

use DB;
use Config;

class Helper
{

public static function DBConnection()
    {
        if( env('APP_ENV') ==  'local' )
        {
            $databse_name = "loc_muft";
            $host = '127.0.0.1';
            $user="root";
            $password="";
        } else{
            $databse_name = 'tic_tac';
            $host = 'localhost';
            $user="";
            $password="";
        }
        $state = true;
        try {
            Config::set('database.connections.db2', array(
                'driver'    => 'mysql',
                'host'      => $host,
                'database'  => $databse_name,
                'username'  => $user,
                'password'  => $password,
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict' => false,
            ));
        /* \DB::setDefaultConnection('db2');
        $state = \DB::connection()->getPdo();*/

            Config::set('database.connections.db2.database', $databse_name);
            //\DB::setDefaultConnection('myConnection');
            \DB::reconnect('db2');

        } catch( \Exception $e) {
            $state = false;
        }
        return $state;
    }

}
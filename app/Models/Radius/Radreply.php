<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models\Radius;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Radreply extends Model
{
    use SoftDeletes;
	
	protected $table = 'radreply';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

}

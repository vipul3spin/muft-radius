<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models\Radius;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Radcheck extends Model
{
    use SoftDeletes;
	
	protected $table = 'radcheck';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

}

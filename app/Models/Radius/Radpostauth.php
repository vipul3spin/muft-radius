<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models\Radius;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Radpostauth extends Model
{
    use SoftDeletes;
	
	protected $table = 'radpostauth';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

}

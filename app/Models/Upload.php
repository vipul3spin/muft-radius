<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use config;

class Upload extends Model
{
    use SoftDeletes;
	
	protected $table = 'uploads';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	/**
     * Get the user that owns upload.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    /**
     * Get File path
     */
    public function path()
    {
        return url("files/".$this->hash."/".$this->name);
    }

    public static function getfilepath($id,$size)
    {
        $img_id = [];//DB::table('employees')->where('id','=',$id)->limit(1)->value('profile_image');
        if(!empty($img_id))
        {
            $user_img = DB::table('uploads')->where('id','=',$img_id)->whereNull('deleted_at')->limit(1)->get();
            if(!empty($user_img))
            {
                $img_url = url("files/".$user_img[0]->hash."?s=".$size);
            } else {
                $img_url = asset('la-assets/img/avatar.png');
            }
        } else {
            $img_url = asset('la-assets/img/avatar.png');
        }
        
        //$fileurl = "files/".$user_img->hash."/".$user_img;
        
        return $img_url;
    }    
}

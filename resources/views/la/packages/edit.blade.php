@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/packages') }}">Package</a> :
@endsection
@section("contentheader_description", $package->$view_col)
@section("section", "Packages")
@section("section_url", url(config('laraadmin.adminRoute') . '/packages'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Packages Edit : ".$package->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($package, ['route' => [config('laraadmin.adminRoute') . '.packages.update', $package->id ], 'method'=>'PUT', 'id' => 'package-edit-form']) !!}
					{{-- @la_form($module) --}}
					
					@la_input($module, 'name') 
					<input type="hidden" name="old_name" value="{{ $package->name }}">
					@la_input($module, 'valid_for')
					@la_input($module, 'bandwidth_up')
					@la_input($module, 'bandwidth_down')
					@la_input($module, 'description')
					{{-- @la_input($module, 'loc_id') --}}

					@if(\Entrust::hasRole('SUPER_ADMIN'))
					<div class="form-group">
						<label for="location">Location* :</label>
						<select class="form-control" required="1" data-placeholder="Select Role" rel="select2" name="loc_id">
							<?php $locas = App\Models\Location::all(); ?>
							@foreach($locas as $loc)
								@if($loc->id == $package->loc_id)
									<option value="{{ $loc->id }}" selected>{{ $loc->name }}</option>
								@else
									<option value="{{ $loc->id }}">{{ $loc->name }}</option>
								@endif
							@endforeach
						</select>
					</div>
					@else
						<input type="hidden" name="loc_id" value="{{ $package->loc_id }}">
					@endif
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-info']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/packages') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#package-edit-form").validate({
		
	});
});
</script>
@endpush

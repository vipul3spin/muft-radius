@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/subscribers') }}">Subscriber</a> :
@endsection
@section("contentheader_description", $subscriber->$view_col)
@section("section", "Subscribers")
@section("section_url", url(config('laraadmin.adminRoute') . '/subscribers'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Subscribers Edit : ".$subscriber->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($subscriber, ['route' => [config('laraadmin.adminRoute') . '.subscribers.update', $subscriber->id ], 'method'=>'PUT', 'id' => 'subscriber-edit-form', 'enctype' => 'multipart/form-data']) !!}
					{{-- @la_form($module) --}}
					
					@la_input($module, 'name')
					{{-- @la_input($module, 'username') --}}
					<div class="form-group">
						<label for="username">Username* :</label>
						<input type="text" class="form-control" disabled value="{{ $subscriber->username }}">
						<input type="hidden" name="username" value="{{ $subscriber->username }}">
					</div>					
					@la_input($module, 'pass')
					@la_input($module, 'confirm_pass')
					<input type="hidden" name="old_pass" value="{{ $subscriber->confirm_pass }}">
					@la_input($module, 'addr')
					@la_input($module, 'city')
					@la_input($module, 'state')
					@la_input($module, 'country')
					@la_input($module, 'zip')
					@la_input($module, 'mobile')
					@la_input($module, 'email')
					@la_input($module, 'status')
					@la_input($module, 'fixed_ip')
					@la_input($module, 'bind_mac')
					@la_input($module, 'mac_addr')
					{{--
					@la_input($module, 'loc_id')
					--}}

					@if(\Entrust::hasRole('SUPER_ADMIN'))
					<div class="form-group">
						<label for="location">Location* :</label>
						<select class="form-control" required="1" data-placeholder="Select Role" rel="select2" name="loc_id">
							<?php $locas = App\Models\Location::all(); ?>
							@foreach($locas as $loc)
								@if($loc->id == $subscriber->loc_id)
									<option value="{{ $loc->id }}" selected>{{ $loc->name }}</option>
								@else
									<option value="{{ $loc->id }}">{{ $loc->name }}</option>
								@endif
							@endforeach
						</select>
					</div>
					@else
						<input type="hidden" name="loc_id" value="{{ $subscriber->loc_id }}">
					@endif
					{{-- @la_input($module, 'kyc_doc') --}}
					<div class="form-group">
						<label class="form-label">KYC Document* :</label><br/>
						<?php $imgtype = array('jpg','JPG','jpeg','JPEG','png','PNG'); ?>
						<?php 
							if($subscriber->kyc_doc <> ""){	
								if(in_array(substr(strrchr($subscriber->kyc_doc,'.'),1) , $imgtype )){
						?>
								<a href="{{ url('kyc/'.$subscriber->kyc_doc) }}" target="_blank">
									<img src="{{ url('kyc/'.$subscriber->kyc_doc) }}" width="20%" style="max-width:100px;">
								</a>
						<?php } else { ?>
								<a href="{{ url('kyc/'.$subscriber->kyc_doc) }}" target="_blank">View KYC</a>
						<?php } 
							} else {
								//echo "NOT Found";
							} 
						?>
						
						<input type="file" name="kyc_doc" value="" placeholder="Please upload a photocopy of your personal Aadhar card / Driving Licence / Voter Card ( jpg, png, pdf)" class="form-control">
						<div id="info">Please upload a photocopy of Aadhar card / Driving Licence / Voter Card ( jpg, png, pdf)</div>
					</div>
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-info']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/subscribers') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#subscriber-edit-form").validate({
		
	});
});
</script>
@endpush

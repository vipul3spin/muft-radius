@extends('la.layouts.app')

@section('htmlheader_title')
	Subscriber View
@endsection


@section('main-content')
<div id="page-content" class="profile2">
	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/subscribers') }}" data-toggle="tooltip" data-placement="right" title="Back to Subscribers"><i class="fa fa-chevron-left"></i></a></li>
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> Details</a></li>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-connection" data-target="#tab-connection"><i class="fa fa-gears"></i> Connection</a></li>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-history" data-target="#tab-history"><i class="fa fa-clock-o"></i> History</a></li>			
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>General Info</h4>
					</div>
					<div class="panel-body">
						@la_display($module, 'name')
						@la_display($module, 'username')
						@la_display($module, 'pass')
						@la_display($module, 'confirm_pass')
						@la_display($module, 'addr')
						@la_display($module, 'city')
						@la_display($module, 'state')
						@la_display($module, 'country')
						@la_display($module, 'zip')
						@la_display($module, 'mobile')
						@la_display($module, 'email')
						@la_display($module, 'status')
						@if(\Entrust::hasRole('SUPER_ADMIN'))
							@la_display($module, 'loc_id')
						@endif
						{{-- @la_input($module, 'kyc_doc') --}}
						<div class="form-group">
							<label for="kyc_doc" class="col-md-2">KYC Document* :</label>
							<?php $imgtype = array('jpg','JPG','jpeg','JPEG','png','PNG'); ?>
							<div class="col-md-10 fvalue">
								<?php 
									if($subscriber->kyc_doc <> ""){	
										if(in_array(substr(strrchr($subscriber->kyc_doc,'.'),1) , $imgtype )){
								?>
										<a href="{{ url('kyc/'.$subscriber->kyc_doc) }}" target="_blank">
											<img src="{{ url('kyc/'.$subscriber->kyc_doc) }}" width="20%" style="max-width:100px;">
										</a>
								<?php } else { ?>
										<a href="{{ url('kyc/'.$subscriber->kyc_doc) }}" target="_blank">View KYC</a>
								<?php } 
									} else {
										echo "NOT Found";
									} 
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane fade in" id="tab-connection">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>Connection Info</h4>
					</div>
					<div class="panel-body">
						@la_display($module, 'fixed_ip')
						@la_display($module, 'bind_mac')
						@la_display($module, 'mac_addr')                                                    
					</div>
				</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane fade in" id="tab-history">
			<div class="tab-content">
				<div class="panel info">
					<div class="panel-body" style="background: #f2f4f6;">
		                <div class="box box-info">
		                        <!--<div class="box-header"></div>-->
		                        <div class="box-body">                                            
		                            <table id="example1" class="table table-bordered" style="width: 100%">
		                            <thead>
		                            <tr class="info">
		                                    @foreach( $listing_cols as $col )
		                                    <th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
		                                    @endforeach
		                            </tr>
		                            </thead>
		                            <tbody>

		                            </tbody>
		                            </table>
		                        </div>
		                </div>                                                    
					</div>
				</div>
			</div>
		</div>				
	</div>
	</div>
	</div>
</div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/subscriber_ajax/'.$subscriber->id) }}",
        language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search"
        },
	order: [[ 0, "desc" ]],
        responsive:true,
        columnDefs: [
               { responsivePriority: 0, targets: 4 }
        ] 
	});
});
</script>
@endpush
@extends("la.layouts.app")

@section("contentheader_title", "Online Subscribers")
@section("contentheader_description", "Online Subscribers listing")
@section("section", "Online Subscribers")
@section("sub_section", "Listing")
@section("htmlheader_title", "Online Subscribers Listing")

@section("headerElems")

@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-info">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="sessiontable" class="table table-bordered">
		<thead>
		<tr class="info">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>

<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<script>
$(function () {
	var table = $("#sessiontable").DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url(config('laraadmin.adminRoute') . '/online_subscriber_dt_ajax') }}",
            language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Search"
            },
	    order: [[ 5, "desc" ]],
            responsive:true,
            @if($show_actions)
                columnDefs: [ {responsivePriority: 0, targets: -1 },{responsivePriority: 1, targets: 2 },{responsivePriority: 2, targets: 5 }],
            @else
                columnDefs: [ {responsivePriority: 0, targets: 0 },{responsivePriority: 1, targets: 2 },{responsivePriority: 2, targets: 5 }],			
            @endif                
	});
});
</script>
@endpush

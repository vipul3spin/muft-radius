@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/locations') }}">Location</a> :
@endsection
@section("contentheader_description", $location->$view_col)
@section("section", "Locations")
@section("section_url", url(config('laraadmin.adminRoute') . '/locations'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Locations Edit : ".$location->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($location, ['route' => [config('laraadmin.adminRoute') . '.locations.update', $location->id ], 'method'=>'PUT', 'id' => 'location-edit-form']) !!}
					{{-- @la_form($module) --}}
					
					@la_input($module, 'name')
					@la_input($module, 'short_name')
					@la_input($module, 'subdomain')
					@la_input($module, 'address')
					@la_input($module, 'city')
					@la_input($module, 'state')
					@la_input($module, 'country')
					@la_input($module, 'contact_person')
					@la_input($module, 'email')
					@la_input($module, 'phone')
					{{-- 
					@la_input($module, 'owner') 
					@la_input($module, 'status')
					--}}
					<input type="hidden" name="owner" value="{{ $location->owner }}">
					<div class="form-group">
						<label for="location">Status* :</label>
						<select class="form-control" required="1" data-placeholder="Select Role" rel="select2" name="status">
							<option value="1" <?php echo ($location->status == 1)? "selected" : '' ; ?>>Enable</option>
							<option value="0" <?php echo ($location->status == 0)? "selected" : '' ; ?>>Disable</option>
						</select>
					</div>
					@la_input($module, 'logo')					
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-info']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/locations') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#location-edit-form").validate({
		
	});
});
</script>
@endpush

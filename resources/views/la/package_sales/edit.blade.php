@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/package_sales') }}">Package Sale</a> :
@endsection
@section("contentheader_description", $package_sale->$view_col)
@section("section", "Package Sales")
@section("section_url", url(config('laraadmin.adminRoute') . '/package_sales'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Package Sales Edit : ".$package_sale->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($package_sale, ['route' => [config('laraadmin.adminRoute') . '.package_sales.update', $package_sale->id ], 'method'=>'PUT', 'id' => 'package_sale-edit-form']) !!}
					{{-- @la_form($module) --}}
					
					@la_input($module, 'subs_id')
					@la_input($module, 'package_id')
					@la_input($module, 'sale_date')
					{{--
					@la_input($module, 'loc_id')
					--}}

					@if(\Entrust::hasRole('SUPER_ADMIN'))
					<div class="form-group">
						<label for="location">Location* :</label>
						<select class="form-control" required="1" data-placeholder="Select Role" rel="select2" name="loc_id">
							<?php $locas = App\Models\Location::all(); ?>
							@foreach($locas as $loc)
								@if($loc->id == $package_sale->loc_id)
									<option value="{{ $loc->id }}" selected>{{ $loc->name }}</option>
								@else
									<option value="{{ $loc->id }}">{{ $loc->name }}</option>
								@endif
							@endforeach
						</select>
					</div>
					@else
						<input type="hidden" name="loc_id" value="{{ $package_sale->loc_id }}">
					@endif					
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-info']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/package_sales') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#package_sale-edit-form").validate({
		
	});
});
</script>
@endpush

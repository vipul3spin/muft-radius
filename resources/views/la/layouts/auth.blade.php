<!DOCTYPE html>
<html>

@include('la.layouts.partials.htmlheader')
@include('la.layouts.partials.flash')
@yield('content')

</html>
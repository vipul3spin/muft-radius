@if(!isset($no_padding))
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        Powered by <a href="https://muftinternet.com" target="_blank">Muft Internet</a>
    </div>
    <strong>Copyright &copy; 2022
</footer>
@endif
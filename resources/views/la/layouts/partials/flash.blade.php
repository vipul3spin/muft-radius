@if(session()->has('success'))
<script>
    var msgtype =  'success';
    var msgtitle =  "{{ session()->get('success') }}";
    var cssclass = 'alert-success';
</script>  
@endif

@if(session()->has('error')) 
<script>
    var msgtype =  'error';
    var msgtitle =  "{{ session()->get('error') }}";
    var cssclass = 'alert-danger';
</script>  
@endif

@if(session()->has('warning'))
<script>    
    var msgtype =  'warning';
    var msgtitle =  "{{ session()->get('warning') }}";
    var cssclass = 'alert-warning';
</script>  
@endif

@if(session()->has('info'))
<script>    
    var msgtype =  'info';
    var msgtitle =  "{{ session()->get('info') }}";
    var cssclass = 'alert-info';
</script>  
@endif

@if(session()->has('success') || session()->has('error') || session()->has('warning') || session()->has('info'))
<script>
swal({
  //position: 'top-right',
  position: 'top',
  toast: true,
  type: msgtype,
  title: msgtitle,
  showConfirmButton: false,
  timer: 5000
})
</script>    
@endif



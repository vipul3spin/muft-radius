@extends("la.layouts.app")

@section("contentheader_title", "NAS Devices")
@section("contentheader_description", "NAS Devices listing")
@section("section", "NAS Devices")
@section("sub_section", "Listing")
@section("htmlheader_title", "NAS Devices Listing")

@section("headerElems")
@la_access("NAS_Devices", "create")
	<button class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add NAS Device</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-info">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="info">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("NAS_Devices", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add NAS Device</h4>
			</div>
			{!! Form::open(['action' => 'LA\NAS_DevicesController@store', 'id' => 'nas_device-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    {{-- @la_form($module) --}}
					
					@la_input($module, 'nas_ip')
					@la_input($module, 'short_name')
					@la_input($module, 'nas_type')
					@la_input($module, 'secret')
					@la_input($module, 'description')
					{{--
					@la_input($module, 'loc_id')
					--}}

					<?php
						$cget_loc = \Cookie::get('loc_id');
					?>
					@if(\Entrust::hasRole('SUPER_ADMIN'))
					<div class="form-group">
						<label for="location">Location* :</label>
						<select class="form-control" required="1" data-placeholder="Select Role" rel="select2" name="loc_id">
							<?php $locas = App\Models\Location::all(); ?>
							@foreach($locas as $loc)
								@if($loc->id == $cget_loc)
									<option value="{{ $loc->id }}" selected>{{ $loc->name }}</option>
								@else
									<option value="{{ $loc->id }}">{{ $loc->name }}</option>
								@endif
							@endforeach
						</select>
					</div>
					@else
						<input type="hidden" name="loc_id" value="<?php echo $cget_loc; ?>">
					@endif

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-info']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/nas_device_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#nas_device-add-form").validate({
		
	});
});
</script>
@endpush

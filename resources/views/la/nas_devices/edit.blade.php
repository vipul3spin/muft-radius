@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/nas_devices') }}">NAS Device</a> :
@endsection
@section("contentheader_description", $nas_device->$view_col)
@section("section", "NAS Devices")
@section("section_url", url(config('laraadmin.adminRoute') . '/nas_devices'))
@section("sub_section", "Edit")

@section("htmlheader_title", "NAS Devices Edit : ".$nas_device->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($nas_device, ['route' => [config('laraadmin.adminRoute') . '.nas_devices.update', $nas_device->id ], 'method'=>'PUT', 'id' => 'nas_device-edit-form']) !!}
					{{-- @la_form($module) --}}
					

					@la_input($module, 'nas_ip')
					@la_input($module, 'short_name')
					@la_input($module, 'nas_type')
					@la_input($module, 'secret')
					@la_input($module, 'description')
					{{--
					@la_input($module, 'loc_id')
					--}}
					@if(\Entrust::hasRole('SUPER_ADMIN'))
					<div class="form-group">
						<label for="location">Location* :</label>
						<select class="form-control" required="1" data-placeholder="Select Role" rel="select2" name="loc_id">
							<?php $locas = App\Models\Location::all(); ?>
							@foreach($locas as $loc)
								@if($loc->id == $nas_device->loc_id)
									<option value="{{ $loc->id }}" selected>{{ $loc->name }}</option>
								@else
									<option value="{{ $loc->id }}">{{ $loc->name }}</option>
								@endif
							@endforeach
						</select>
					</div>
					@else
						<input type="hidden" name="loc_id" value="{{ $nas_device->loc_id }}">
					@endif

                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-info']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/nas_devices') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#nas_device-edit-form").validate({
		
	});
});
</script>
@endpush
